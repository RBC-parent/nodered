FROM nodered/node-red-docker:latest
RUN npm install node-red-node-wordpos
RUN npm install node-red-contrib-web-worldmap
RUN npm install node-red-contrib-cpu
RUN npm install node-red-node-darksky
RUN npm install node-red-contrib-sun-position
RUN npm install node-red-dashboard
RUN npm install node-red-node-twilio
RUN npm install node-red-contrib-pythonshell
